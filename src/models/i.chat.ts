import { Usuario } from './i.usuario';

export interface Messages {
    emisor: string;
    receptor: string;
    fecha: Date;
    mensaje: string;
}

export interface Chat {
    estado : number;
    fechaFin: Date;
    fechaInicio: Date;
    mensajes: Messages[];
    usuarioEmisor: Usuario;
    usuarioReceptor: Usuario;
}

export interface IQuery {
    idField: string;
    compare: '==' | '=!' | any;
    value: string;
}