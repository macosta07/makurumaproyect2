import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/service/user/service.service';
import { environment } from 'src/environments/environment';
import { Usuario } from 'src/models/i.usuario';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss'],
})
export class ProfileViewComponent implements OnInit {

  public skills: string[] = [];
  public user: Usuario;
  
  @ViewChild('inputSkills') inputSkills: any;
  @Input() IsEditable: boolean;
  @Output() SendEditable = new EventEmitter<boolean>();

  constructor(private userService: ServiceService, private route: Router, public alertController: AlertController) {
    this.user = {
      id: '',
      perfil: '',
      correo: '',
      nombre: '',
      skills: this.skills,
      telefono: 0
    };
  }

  ngOnInit() {
    this.skills = [];
    this.getProfile();
  }

  public keyValueEvent(skill) {
    console.log(skill);
    const tmp = skill;
    this.skills.push(tmp);
    this.inputSkills.value = '';
    console.log(this.inputSkills.value);
  }

  private getProfile(): void {
    // invoca desde profile-init
    if (!this.IsEditable) {
      // ir a firebase
      this.userService.getUser().subscribe(
        (user) => {
          console.log('user', user);
          this.user = user;
          this.skills = this.user.skills;
        }
      );
    } else {
      this.user.correo = localStorage.getItem(environment.application.email);
      this.user.id = localStorage.getItem(environment.application.email);
      this.user.telefono = parseInt(localStorage.getItem(environment.application.phone));
    }
  }

  public setProfile(): void {
    this.user.skills = this.skills;
    console.log(this.user);
    this.userService.setUser(this.user).then(
      (smsSuccess) => {
        console.log('Data success service' , smsSuccess);
        localStorage.setItem(environment.application.isRegister , 'true');
        this.IsEditable = false;
        this.SendEditable.emit(this.IsEditable);
        this.route.navigateByUrl('menu');
      }
    );
  }

  public removeChip(index: number): void {
    if (this.IsEditable) {    
      console.log(index);
      this.skills.splice(index , 1);
    }
  }
}
