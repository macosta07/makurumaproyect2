import { IQuery, Messages } from 'src/models/i.chat';
import { environment } from 'src/environments/environment';
import { FirebaseService } from './../firebase/service.service';
import { Injectable } from '@angular/core';
import { Usuario } from 'src/models/i.usuario';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private service: FirebaseService) { }

  public searchMessages(userInput: Usuario , userOutput: Usuario){​​

    const query: IQuery[] = [

      {​​

        compare : 'in',

        idField : 'emisor',

        value : userInput.correo

      }​​,

      {​​

        compare : '==',

        idField : 'receptor',

        value : userOutput.correo

      }​​

    ];

    return this.service.findDataQueryChat(environment.collections.chat , query );

  }​​

  public pushMessage(message: Messages) {
    return this.service.setDataWithoutId(environment.collections.chat , message);
  }
}
