import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from 'src/models/i.usuario';
import { FirebaseService } from '../firebase/service.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private firebase: FirebaseService) { }

  public getUser(): Observable<any> {
    const email = localStorage.getItem(environment.application.email);
    return this.firebase.getDataWithId(email , environment.collections.user);
  }

  public getAllUser(): Observable<Usuario[]>{
    return this.firebase.getAllDocuments(environment.collections.user);
  }

  public setUser(user: Usuario): Promise<any> {
    return this.firebase.setData(user, environment.collections.user);
  }
//Servicio en la capausuario para autenticación con numero de telefono 
  public setPhoneNumber(number: string , verifier: any ): Promise<any> {
    return this.firebase.sendAuthSmsFirebase(number , verifier);
  }
// servivicio autenticación con google 
  public loginWithGoogle(): Promise<any>{
    return this.firebase.loginWithGoogle();
  }

  public loginWithMicrosoft(): Promise<any> {
    return this.firebase.loginWithMicrosoft();
  }
}
