import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabProfileEditionPage } from './tab-profile-edition.page';

describe('TabProfileEditionPage', () => {
  let component: TabProfileEditionPage;
  let fixture: ComponentFixture<TabProfileEditionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabProfileEditionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabProfileEditionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
