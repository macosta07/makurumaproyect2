import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabProfileEditionPageRoutingModule } from './tab-profile-edition-routing.module';

import { TabProfileEditionPage } from './tab-profile-edition.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabProfileEditionPageRoutingModule
  ],
  declarations: [TabProfileEditionPage]
})
export class TabProfileEditionPageModule {}
