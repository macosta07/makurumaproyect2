import { ChatService } from './../service/chat/chat.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Usuario } from 'src/models/i.usuario';
import { Chat, Messages } from 'src/models/i.chat';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {

  public userEmisor: Usuario;
  public userReceptor: Usuario;
  public messages: Messages[];
  public messageInput: string = '';


  constructor ( private chatService: ChatService) {  }

  ngOnInit() {
    this.loadMessage();
  }

  private loadMessage(): void {
    console.log('entrando a mensages');
    this.userEmisor = {
      correo : localStorage.getItem('email_user'),
      id: '', nombre: '', perfil : '', skills: [], telefono: 0
    };

    this.userReceptor  = {
      correo : sessionStorage.getItem('chat_with'),
      nombre: sessionStorage.getItem('chat_name'),
      id: '', perfil : '', skills: [], telefono: 321456789
    };

    this.chatService.searchMessages( this.userEmisor , this.userReceptor ).subscribe(
      (result)=> {
        console.log('result' , result);
        this.messages = result;
      }
    );
  }

  public sendMessage(): void {
    const msm: Messages = {
      emisor: this.userEmisor.correo,
      receptor:  this.userReceptor.correo,
      fecha: new Date(),
      mensaje: this.messageInput
    }
    this.chatService.pushMessage(msm);
  }

}
