// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyBILDXCvyFBIGabofCaFPFsdIPqysp01Z0',
    authDomain: 'makurumaproyect2.firebaseapp.com',
    databaseURL: 'https://makurumaproyect2.firebaseio.com',
    projectId: 'makurumaproyect2',
    storageBucket: 'makurumaproyect2.appspot.com',
    messagingSenderId: '293364252742',
    appId: '1:293364252742:web:c09472b45f13234fc4ca47',
    measurementId: 'G-BL7SPT3FJP'
  },
  application : {
    phone: 'phone_user',
    email: 'email_user',
    isRegister: 'register_user'
  },
  collections: {
    user: 'Usuarios',
    chat: 'Chats'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
