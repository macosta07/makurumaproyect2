export const environment = {
  production: true,
  firebaseConfig : {
    apiKey: 'AIzaSyBILDXCvyFBIGabofCaFPFsdIPqysp01Z0',
    authDomain: 'makurumaproyect2.firebaseapp.com',
    databaseURL: 'https://makurumaproyect2.firebaseio.com',
    projectId: 'makurumaproyect2',
    storageBucket: 'makurumaproyect2.appspot.com',
    messagingSenderId: '293364252742',
    appId: '1:293364252742:web:c09472b45f13234fc4ca47',
    measurementId: 'G-BL7SPT3FJP'
  },
  application : {
    phone: 'phone_user',
    email: 'email_user',
    isRegister: 'register_user'
  },
  collections: {
    user: 'Usuarios',
    chat: 'Chats'
  }
};
